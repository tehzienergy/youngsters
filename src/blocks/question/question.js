$('.question__header').click(function(e) {
  e.preventDefault();
  $(this).toggleClass('question__header--active');
  $(this).closest('.question').find('.question__content').slideToggle('fast');
})
