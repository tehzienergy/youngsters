$('.material__dscr').ellipsis({
  lines: 3,
  ellipClass: 'ellip',
  responsive: true      // set to true if you want ellipsis to update on window resize. Default is false
});