if ($('.scrollbar--x').length) {

  $('.scrollbar--x').each(function() {
    const ps = new PerfectScrollbar(this, {
      suppressScrollY: true,
      useBothWheelAxes: true,
    });  
  })
}
