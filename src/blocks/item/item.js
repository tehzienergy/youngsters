//iTitleHeight = function() {
//  var itemHeader = $(".item__content-top h2");
//  itemHeader.css('min-height', '')
//  maxHeight = 0;
//  var heights = itemHeader.map(function () {
//    return $(this).height();
//  }).get();
//
//  maxHeight = Math.max.apply(null, heights);
//  itemHeader.css('min-height', maxHeight);
//}
//
//$(window).on('load resize', function () {
//  iTitleHeight();
//})


if ($(window).width() > 767) {
  itemHeight = function() {
    var item = $(".item");
    item.css('min-height', '')
    maxHeight = 0;
    var heights = item.map(function () {
      return $(this).height();
    }).get();

    maxHeight = Math.max.apply(null, heights);
    item.css('min-height', maxHeight);
  }

  $(window).on('load resize', function () {
    itemHeight();
  })
}
