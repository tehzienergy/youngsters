$('.select__input').select2({
  minimumResultsForSearch: -1,
  width: '100%',
  dropdownAutoWidth: true,
  placeholder: function() {
    $(this).data('placeholder');
  }
});

$('.modal .select__input').select2({
  minimumResultsForSearch: -1,
  width: '100%',
  dropdownAutoWidth: true,
  dropdownParent: $('.modal .select'),
  placeholder: function() {
    $(this).data('placeholder');
  }
});

$('.select--birth .select__input').select2({
  minimumResultsForSearch: -1,
  width: '100%',
  dropdownAutoWidth: true,
  dropdownCssClass: 'select2-dropdown--birth',
  placeholder: function() {
    $(this).data('placeholder');
  }
});
