$('.js-fancybox-close').click(function(e) {
  e.preventDefault();
  $.fancybox.close();
});

$('[data-fancybox]').fancybox({
  touch: false
});

$('.chat__close').click(function(e) {
  e.preventDefault();
  $.fancybox.close();
})


$('.js-modal-gallery').fancybox({
  touch: false,
  afterLoad: function(instance, slide) {
    $('.modal__gallery').slick({
      slidesToShow: 1,
      fade: true,
      infinite: false
    });
  }
});

$('.modal__userpic-controls').slick({
  slidesToShow: 3,
  slidesToScroll: 1,
  asNavFor: '.modal__userpic-main',
  dots: false,
  arrows: false,
  focusOnSelect: true,
  arrows: true,
  responsive: [
    {
      breakpoint: 567,
      settings: {
        dots: true,
        arrows: false
      }
    },
  ]
});

$('.modal__userpic-main').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: false,
  fade: true,
  centerMode: true,
  asNavFor: '.modal__userpic-controls'
});
