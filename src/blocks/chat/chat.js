if ($('.scrollbar-y').length) {

  $('.scrollbar-y').each(function() {
    const ps2 = new PerfectScrollbar(this, {
      useBothWheelAxes: true,
    });  
  })
};

$('.chat__search-btn').click(function(e) {
  e.preventDefault();
  $(this).closest('.chat__search').toggleClass('chat__search--active');
});


/* Для демонстрации */
$('.person').click(function(e) {
  e.preventDefault();
  $('.chat__main-wrapper').toggleClass('chat__main-wrapper--active');
})