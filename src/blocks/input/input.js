if ($(".input--calendar")[0]){
  $(function() {
    $('.input--calendar').datepicker({
      yearRange: "-100:+3",
      defaultDate: null,
      changeMonth: true,
      changeYear: true,
      dateFormat: "dd.mm.yy",
      numberOfMonths: 1,
      firstDay: 1
    });
  });
}

$(".input--auto").each(function() {
  var item = $(this);

  item.easyAutocomplete({

    //url: "resources/countries.json",

    data: ["Париж", "Лондон", "Саламанка", "Сан-Себастьян", "Санта Круз де Тенерифе"],

    list: {
      match: {
        enabled: true
      },
      onShowListEvent: function() {
        item.closest('.easy-autocomplete').addClass('active');
//        $(this).addClass('active');
      },
      onHideListEvent: function() {
        item.closest('.easy-autocomplete').removeClass('active');
      },
    },

    theme: "square"
  });
});
