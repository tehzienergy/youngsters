$('.slider').slick({
  slidesToShow: 1,
  responsive: [
    {
      breakpoint: 1200,
      settings: {
        arrows: false,
        dots: true
      }
    },
  ]
});
